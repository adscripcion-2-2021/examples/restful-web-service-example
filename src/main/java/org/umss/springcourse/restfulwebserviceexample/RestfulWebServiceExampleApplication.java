package org.umss.springcourse.restfulwebserviceexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class RestfulWebServiceExampleApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext applicationContext = SpringApplication.run(RestfulWebServiceExampleApplication.class, args);
		String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
		for (String name: beanDefinitionNames) {
			System.out.println(name);
		}
	}

}
