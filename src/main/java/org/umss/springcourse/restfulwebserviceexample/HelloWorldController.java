package org.umss.springcourse.restfulwebserviceexample;

import org.springframework.web.bind.annotation.*;

@RestController
public class HelloWorldController {

    @GetMapping("/hello-world")
    public String helloWorld() {
        return "Hello World";
    }

    @GetMapping("/hello-world-bean")
    public HelloWorldBean helloWorldBean() {
        return new HelloWorldBean("Hello World");
    }

    @GetMapping("/hello-world-path/{name}/test/{test}")
    public HelloWorldBean helloWorldPath(@PathVariable String name, @PathVariable String test) {
        return new HelloWorldBean(String.format("Hello World, %s, %s", name, test));
    }
}
